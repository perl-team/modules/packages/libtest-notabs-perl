libtest-notabs-perl (2.02-2) UNRELEASED; urgency=medium

  * debian/watch: use uscan version 4.

 -- gregor herrmann <gregoa@debian.org>  Sat, 22 Feb 2020 16:46:07 +0100

libtest-notabs-perl (2.02-1) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 2.02.
  * Update years of packaging copyright.
  * Update upstream contact.
  * Declare compliance with Debian Policy 4.1.4.
  * Bump debhelper compatibility level to 10.
  * Update build dependencies.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 May 2018 18:03:40 +0200

libtest-notabs-perl (2.00-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  * Import upstream version 2.00.
  * Update debian/upstream/metadata.
  * Install new CONTRIBUTING file.
  * debian/copyright: drop stanza about removed third-party files.
  * Declare compliance with Debian Policy 4.0.0.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Thu, 03 Aug 2017 22:08:01 -0400

libtest-notabs-perl (1.4-1) unstable; urgency=low

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * debian/control: remove Nicholas Bamber from Uploaders on request of
    the MIA team.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Import upstream version 1.4
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Fri, 04 Sep 2015 23:16:38 +0200

libtest-notabs-perl (1.3-1) unstable; urgency=low

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Wed, 27 Jun 2012 19:40:57 +0200

libtest-notabs-perl (1.2-1) unstable; urgency=low

  * Team upload.

  [ Nicholas Bamber ]
  * Added myself to Uploaders
  * New upstream release 1.1
  * Raised standards version to 3.9.2

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * New upstream release 1.2.
  * debian/copyright: update to Copyright-Format 1.0.
  * Update years of copyright for inc/*, refresh license stanzas.
  * Bump Standards-Version to 3.9.3 (no changes).
  * Set debhelper compatibility level to 8.

 -- gregor herrmann <gregoa@debian.org>  Sat, 16 Jun 2012 17:34:14 +0200

libtest-notabs-perl (1.0-1) unstable; urgency=low

  * Initial Release (Closes: #580533)

 -- Jonathan Yu <jawnsy@cpan.org>  Thu, 06 May 2010 16:19:44 -0400
